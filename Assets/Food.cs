﻿using UnityEngine;
using System.Collections;

public class Food : MonoBehaviour {
	public float slow = 0f;
	public int cut = 0;

	public void UpdateSnake(SnakeControl snake){
		snake.timeBetweenMoves += slow;
		int realCut = Mathf.Min (Mathf.RoundToInt(snake.snakeProc.length) - 2, cut);
		snake.snakeProc.length -= realCut;
		snake.snakeProc.curveT += realCut;
	}
}
