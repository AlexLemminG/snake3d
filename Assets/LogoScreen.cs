﻿using UnityEngine;
using System.Collections;

public class LogoScreen : MonoBehaviour {
	public float time = 1f;
	// Use this for initialization
	void Start () {
		Invoke ("ToMenu", time);
	}

	void ToMenu(){
		FindObjectOfType<UIScript> ().ToMainMenu ();
	}
}
