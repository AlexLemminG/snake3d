﻿using UnityEngine;
using System.Collections;

public class UpDownSineAnimation : MonoBehaviour {
	public float freq;
	public float amplitude;
	// Use this for initialization
	Vector3 posInit;
	void Start () {
		posInit = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = posInit + new Vector3 (0f, amplitude * Mathf.Sin (freq * Mathf.PI * 2f * Time.time), 0f);
	}
}
