﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using System;

public class LevelScroll : MonoBehaviour {
	public GameObject levelIconPrefab;
	public GameObject content;
	public float distanceX;
	public Vector2 firstLevelIconPosition;

	public int firstLevelID = 1;
	public int lastLevelID = 15;
	public float margin = 50f;
	public bool random = false;

	//public List<string> p;

	public System.Collections.Generic.Dictionary<string, string> dic;
	// Use this for initialization
	void Start () {
		
		float x = firstLevelIconPosition.x+margin;
		float y = firstLevelIconPosition.y;
		for(int i = content.transform.childCount-1; i >= 0; i--){
			Destroy (content.transform.GetChild (i).gameObject);
			//print ("destr");
		}
		
		//SceneManager.LoadSceneAsync(firstLevelID, LoadSceneMode.Additive);
		for (int i = firstLevelID; i <= lastLevelID; i++) {
			GameObject icon = Instantiate<GameObject> (levelIconPrefab);
			icon.transform.SetParent (content.transform);
			icon.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (x, y);
			var comp = icon.GetComponent<LevelIcon> ();
			comp.showNumber = i - firstLevelID + 1;
			comp.sceneBuildId = i;

			icon.GetComponent<LevelIcon> ().random = random;


			//print ("");
			x += distanceX;
		}
		content.GetComponent<RectTransform> ().sizeDelta = new Vector2 (x-distanceX+margin, 200f);
	}

}
