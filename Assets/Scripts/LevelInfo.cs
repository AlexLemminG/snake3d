﻿using UnityEngine;
using System.Collections;

public class LevelInfo : MonoBehaviour {
	public int apples1;
	public int apples2;
	public int apples3;
	public bool auto = true;

	// Use this for initialization
	void Start () {
		if (auto) {
			apples3 = FindObjectOfType<RandomField> ().CountFood ();
			apples2 = Mathf.CeilToInt(apples3 / 2f + 0.0001f);
			apples1 = Mathf.CeilToInt(apples3 * 1 / 3f);
			apples1 = 1;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public int GetStars(){
		var applesEaten = FindObjectOfType<SnakeControl> ().applesEaten;
		if (applesEaten >= apples3)
			return 3;
		else {
			if(applesEaten >= apples2){
				return 2;
			}else{
				if(applesEaten >= apples1){
					return 1;
				}else{
					return 0;
				}
			}
		}
	}

	void OnDestroy(){
		//int apples;
		//apples = FindObjectOfType<SnakeControl> ().applesEaten;

		//int stars = GetStars (apples);
		//print (stars + " stars");

	}
}
