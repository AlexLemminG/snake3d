﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Wall : MonoBehaviour {

	public enum WallType
	{
		PrototypeWall, GrassWall, GrassGrassWall, StoneWall
	}

	public WallType wallType;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		#if UNITY_EDITOR
		if (!Application.isPlaying) {
			GameObject wallPrefab;
			switch (wallType) {
			case WallType.GrassWall:
				{
					wallPrefab = Resources.Load<GameObject> ("Walls/GrassWall");
					break;
				}

			case WallType.PrototypeWall:
				{
					wallPrefab = Resources.Load<GameObject> ("Walls/PrototypeWall");
					break;
				}

			case WallType.GrassGrassWall:
				{
					wallPrefab = Resources.Load<GameObject> ("Walls/GrassGrassWall");
					break;
				}

			case WallType.StoneWall:
				{
					wallPrefab = Resources.Load<GameObject> ("Walls/StoneWall");
					break;
				}

			default :
				wallPrefab = Resources.Load<GameObject> ("Walls/PrototypeWall");
				break;
			}

			UnityEditorInternal.ComponentUtility.CopyComponent (wallPrefab.GetComponent<MeshFilter> ());
			UnityEditorInternal.ComponentUtility.PasteComponentValues (gameObject.GetComponent<MeshFilter> ());

			UnityEditorInternal.ComponentUtility.CopyComponent (wallPrefab.GetComponent<MeshRenderer> ());
			UnityEditorInternal.ComponentUtility.PasteComponentValues (gameObject.GetComponent<MeshRenderer> ());
		}
		#endif
	}
}
