﻿using UnityEngine;
using System.Collections;

public class RailwayCameraMovement : MonoBehaviour {
	public SnakeProcedural snake;
	public Curve railway;

	Vector3 delta;
	// Use this for initialization
	void Start () {
		delta = Vector3.zero;
		if (snake == null) {
			snake = FindObjectOfType<SnakeProcedural> ();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(delta == Vector3.zero)
			delta = transform.position - ClampedToCurve ();

		var currentDelta = -ClampedToCurve () - delta + transform.position;
		acceleration = currentDelta.magnitude - 1f;
		velocity += acceleration * Time.deltaTime;
		velocity = Mathf.Clamp (velocity, 0f, maxVelocity);

		transform.position = Vector3.MoveTowards (transform.position, ClampedToCurve () + delta, Time.deltaTime * velocity); 
	}
	float velocity;
	float acceleration = 0f;
	float maxVelocity = 5f;
	Vector3 ClampedToCurve(){
		return railway.GetPosAt(railway.ClosestT (ToPos()));
	}

	public Vector3 ToPos(){
		float t = snake.curveT + snake.length - 0.5f;
		Vector3 pos = snake.middleCurve.GetPosAt (t);
		Debug.DrawRay (pos, Vector3.up * 10f);
		return pos;
	}
}
