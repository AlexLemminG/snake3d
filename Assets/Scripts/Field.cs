﻿using UnityEngine;
using System.Collections.Generic;

public class Field : MonoBehaviour {
	GameObject allStatic;

	Dictionary<Vector3, GameObject> staticsDict;
	// Use this for initialization
	void Start () {
		allStatic = new GameObject ("All Static");
		allStatic.transform.parent = transform;
		List<GameObject> statics = new List<GameObject> (); 
		foreach (Transform t in transform.GetComponentsInChildren<Transform>()) {
			if (t.gameObject.isStatic) {
				statics.Add (t.gameObject);
			}
		}
		staticsDict = new Dictionary<Vector3, GameObject> ();
		foreach (GameObject o in statics) {
			if (o.CompareTag("Untagged"))
				continue;
			o.transform.parent = allStatic.transform;
			var pos = o.transform.position;
			pos *= 2f;

			int x = Mathf.RoundToInt (pos.x);
			int y = Mathf.RoundToInt (pos.y);
			int z = Mathf.RoundToInt (pos.z);
			pos = new Vector3 (x, y, z);
			if (staticsDict.ContainsKey (pos)) {
				//print ("da ti zaebal");
			}else
				staticsDict.Add (pos, o);
		}
	}

	public Dictionary<Vector3, GameObject> GetObjectDictionary(){
		Dictionary<Vector3, GameObject> result = new Dictionary<Vector3, GameObject> ();
		foreach (Transform t in GetComponentsInChildren<Transform>()) {
			if (!t.CompareTag ("Untagged")) {
				var p = new Vector3 (Mathf.RoundToInt (t.position.x * 2f) / 2f, Mathf.RoundToInt (t.position.y * 2f) / 2f, Mathf.RoundToInt (t.position.z * 2f) / 2f);
				if(!result.ContainsKey(p))
					result.Add (p, t.gameObject); 

			}
		}
		return result;
	}

	public int foodCount;
	// Update is called once per frame
	void Update () {
		
	}

	public List<GameObject> GetAt(Vector3 at){

		at = at * 2f;
		int x = Mathf.RoundToInt (at.x);
		int y = Mathf.RoundToInt (at.y);
		int z = Mathf.RoundToInt (at.z);
		var atInt = new Vector3 (x, y, z);
		List<GameObject> result = new List<GameObject>();
		if (staticsDict.ContainsKey(atInt)) {
			var val = staticsDict [atInt];
			if(val != null)
				result.Add (val);
		}
		foreach(Transform parent in transform){
			if(parent.gameObject != allStatic)
				foreach (Transform t in parent.GetComponentsInChildren<Transform>()) {
					if (t.CompareTag("Untagged"))
						continue;
					Vector3 pos = t.position * 2f;
					if(Mathf.RoundToInt(pos.x)==x && Mathf.RoundToInt(pos.y)==y && Mathf.RoundToInt(pos.z)==z){
						if(!result.Contains(t.gameObject))
							result.Add (t.gameObject);
					}
				}
		}
		return result;
	}

}
