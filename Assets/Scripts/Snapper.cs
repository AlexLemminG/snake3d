﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Snapper : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(!Application.isPlaying){
			var v = transform.localPosition;
			transform.localPosition = new Vector3 (Mathf.RoundToInt (v.x*2f), Mathf.RoundToInt (v.y*2f), Mathf.RoundToInt (v.z*2f)) / 2f;
		}
	}
}
