﻿using UnityEngine;
using System.Collections;

public class BackToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown ("Cancel")) {
			UIScript ui = FindObjectOfType<UIScript>();
			if (ui.IsPaused ()) {
				ui.ToMainMenu ();
			} else {
				ui.ShowPause ();
			}
		}
	}
}
