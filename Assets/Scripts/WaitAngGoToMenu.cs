﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class WaitAngGoToMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Invoke ("GoToMenu", 5f);
	}

	void GoToMenu(){
		SceneManager.LoadScene ("MainMenu");
	}
}
