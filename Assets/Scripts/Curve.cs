﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class Curve : MonoBehaviour {
	[HideInInspector]
	public float lastT = 1f;
	public virtual Vector3 GetPosAt(float t){
		return transform.TransformPoint (Vector3.forward*(t-0.5f));
	}

	public virtual Quaternion GetRotAt(float t){
		return transform.rotation;
	}

	/*
	public void OnDrawGizmos(){
		Gizmos.color = Color.white;
		Gizmos.DrawWireSphere (GetPosAt (0f), 0.1f);
		for (float t = 0; t < lastT; t += 0.01f*lastT) {
			Gizmos.DrawLine (GetPosAt (t), GetPosAt (t + 0.01f*lastT));
		}
	}*/

	public void OnDrawGizmosSelected(){
		Gizmos.color = Color.black;
		Gizmos.DrawWireSphere (GetPosAt (0f), 0.1f);
		for (float t = 0; t < lastT; t += 0.01f*lastT) {
			Gizmos.DrawLine (GetPosAt (t), GetPosAt (t + 0.01f*lastT));
		}
	}


	public float ClosestT(Vector3 point){
		if (true) {
			//return ClosestTExp(point);
		}
		float a = 0f;
		float b = lastT;
		float c = (a + b) / 2;

		Vector3 pA = GetPosAt (a);
		Vector3 pB = GetPosAt (b);
		Vector3 pC = GetPosAt (c);

		float distA = Vector3.Distance (pA, point);
		float distB = Vector3.Distance (pB, point);
		float distC = Vector3.Distance (pC, point);

		float delta = lastT;
		while (delta > 0.0001f) {
			float min = Mathf.Min (distA, distB, distC);
			if (distC == min) {
				a = (a+c)/2;
				b = (b+c)/2;
			}else
				if (distA == min) {
					b = c;
					c = (a + b) / 2;
				}else
					if (distB == min) {
						a = c;
						c = (a + b) / 2;
					}



			delta = (b - a);

			pA = GetPosAt (a);
			pB = GetPosAt (b);
			pC = GetPosAt (c);

			distA = Vector3.Distance (pA, point);
			distB = Vector3.Distance (pB, point);
			distC = Vector3.Distance (pC, point);
		}
		//Debug.DrawRay (GetPosAt (c), transform.up * 10f); 
		return c;
	}

	public float ClosestTExp(Vector3 pos){
		float res = 0f;
		float dist = 10000000f;
		for (float t = 0; t < lastT; t += 0.01f) {
			Vector3 p = GetPosAt (t);
			float currentDist = Vector3.Distance (p, pos);
			if (currentDist < dist) {
				res = t;
				dist = currentDist;
			}
		}
		return res;
	}

}

