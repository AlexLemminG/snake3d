﻿using UnityEngine;
using System.Collections;

public class ShowTotalApples : MonoBehaviour {

	// Use this for initialization
	void Start () {
		UnityEngine.UI.Text text = GetComponent<UnityEngine.UI.Text> ();
		text.text = ": " + PlayerPrefs.GetInt ("Total apples") + "/"+77;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
