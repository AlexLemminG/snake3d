﻿using UnityEngine;
using System.Collections;

public class NothingSelectedToFirstSelected : MonoBehaviour {
	UnityEngine.EventSystems.EventSystem es;
	// Use this for initialization
	void Start () {
		es = GetComponent<UnityEngine.EventSystems.EventSystem> ();
	}
	
	// Update is called once per frame
	void Update () {
		if(es.currentSelectedGameObject == null)
			es.SetSelectedGameObject (es.firstSelectedGameObject);
	}
}
