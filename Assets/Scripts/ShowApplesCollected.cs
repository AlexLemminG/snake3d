﻿using UnityEngine;
using System.Collections;

public class ShowApplesCollected : MonoBehaviour {
	public GameObject apple1;
	public GameObject apple2;
	public GameObject apple3;

	public float showDelay = 0.5f;
	int collected;
	int prevShowed = 0;
	void OnEnable(){
		prevShowed = 0;
		collected = FindObjectOfType<LevelInfo> ().GetStars ();
		InvokeRepeating ("ShowApple", showDelay, showDelay);
	}

	void ShowApple(){
		if (prevShowed < collected) {
			prevShowed++;
			if (prevShowed == 1) {
				apple1.SetActive (true);
			}
			if (prevShowed == 2) {
				apple2.SetActive (true);
			}
			if (prevShowed == 3) {
				apple3.SetActive (true);
			}
			FindObjectOfType<UIScript> ().PlayPick ();
		} else {
			CancelInvoke ();
		}
	}

}
