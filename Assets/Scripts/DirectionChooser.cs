﻿using UnityEngine;
using System.Collections;

public class DirectionChooser : MonoBehaviour {
	SupremeCurve curve;
	// Use this for initialization
	void Start () {
		curve = GetComponent<SupremeCurve> ();
	}
	
	// Update is called once per frame
	void Update () {
		/*
		if(Input.GetKeyDown(KeyCode.Space)){
			CreateByDirection (HalfCircleCurve.Direction.forward);
		}
		if(Input.GetKeyDown(KeyCode.LeftArrow)){
			CreateByDirection (HalfCircleCurve.Direction.left);
		}
		if(Input.GetKeyDown(KeyCode.RightArrow)){
			CreateByDirection (HalfCircleCurve.Direction.right);
		}
		if(Input.GetKeyDown(KeyCode.UpArrow)){
			CreateByDirection (HalfCircleCurve.Direction.top);
		}
		if(Input.GetKeyDown(KeyCode.DownArrow)){
			CreateByDirection (HalfCircleCurve.Direction.bot);
		}*/
	}

	public HalfCircleCurve CreateByDirection(HalfCircleCurve.Direction dir){
		//Destroy(transform.GetChild(transform.childCount-1).gameObject);


		var oCreated = transform.GetChild(transform.childCount-1).gameObject;
		oCreated.GetComponent<HalfCircleCurve> ().dir = dir;
		oCreated.GetComponent<HalfCircleCurve> ().halfUp = false;
		//oCreated.GetComponent<HalfCircleCurve> ().halfDown = false;
		//o.transform.rotation = curve.GetRotAt (curve.transform.childCount);
		//o.transform.position = curve.GetPosAt (curve.transform.childCount);
		//o.transform.position += o.transform.forward * 0.5f;
		//o.transform.parent = transform;

		var o = new GameObject ("CurvePart");
		o.AddComponent<HalfCircleCurve> ().dir = HalfCircleCurve.Direction.forward;
		o.transform.rotation = curve.GetRotAt (curve.transform.childCount);
		o.transform.position = curve.GetPosAt (curve.transform.childCount);
		o.transform.position += o.transform.forward * 0.5f;
		o.transform.parent = transform;


		return oCreated.GetComponent<HalfCircleCurve> ();
	}
}
