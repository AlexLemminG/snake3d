﻿using UnityEngine;
using System.Collections;

public class RotateRandom : MonoBehaviour {

	// Use this for initialization
	void Start () {
		transform.eulerAngles = transform.eulerAngles + new Vector3 (0f, Random.Range (0f, 360f), 0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
