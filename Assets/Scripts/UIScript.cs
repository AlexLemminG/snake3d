﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
#if !UNITY_WEBGL
using UnityEngine.Advertisements;
#endif


public class UIScript : MonoBehaviour {
	SnakeControl snake;
	new AudioSource audio;
	bool unmute;
	void Start(){
		//print (SceneManager.GetActiveScene().name + " loaded");
		snake = FindObjectOfType<SnakeControl> ();
		audio = GetComponent<AudioSource> ();
		//UnityEngine.UI.Toggle unmute = transform.GetComponentInChildren<UnityEngine.UI.Toggle> ();
		unmute = PlayerPrefs.HasKey ("Unmute") ? PlayerPrefs.GetInt ("Unmute") > 0 : true;

		Unmute (unmute);



		UpdateAudioButton ();
	}

	public AudioClip win;
	public AudioClip lose;
	public AudioClip pick;
	public AudioClip move;
	public AudioClip dzin;

	void UpdateAudioButton(){
		if (pausePanel) {
			var audioOn = pausePanel.transform.FindChild ("AudioOn").gameObject;
			var audioOff = pausePanel.transform.Find ("AudioOff").gameObject;

			audioOff.SetActive (!unmute);
			audioOn.SetActive (unmute);
			//print ("audio " + (unmute ? "on" : "off"));
		}
	}


	public void Restart(){
		SceneManager.LoadSceneAsync (SceneManager.GetActiveScene().name);
		CanShowAds ();
	}

	public void SetSkin(string skin){
		var newSkin = (skin == "Tron" ? SnakeSummary.Skin.Tron : (skin == "Rainbow" ? SnakeSummary.Skin.Rainbow : SnakeSummary.Skin.Yellow));
		PlayerPrefs.SetInt ("Skin", (int)newSkin);
		//print (newSkin);
	}

	public GameObject pausePanel;
	public void ShowPause(){
		paused = true;
		pausePanel.SetActive(true);
		snake.enabled = false;
	}

	public void HidePause(){
		paused = false;
		pausePanel.SetActive(false);
		snake.enabled = true;
	}
	bool paused = false;
	public bool IsPaused(){
		return paused;
	}

	public void ToMainMenu(){
		SceneManager.LoadSceneAsync ("NeuMenu");
	}

	public void ToOptions(){
		SceneManager.LoadSceneAsync ("NeuOptions");
	}
	public void RandomLevelSelection(){
		SceneManager.LoadSceneAsync ("NeuLevelSelectionRandom");
	}
	/*
	public void StoryLevel(){
		int lastFinished = PlayerPrefs.HasKey("LastFinished") ? PlayerPrefs.GetInt ("LastFinished") : 0;
		if (lastFinished == 0 || lastFinished == 17)
			SceneManager.LoadScene ("snake301");
		else
			SceneManager.LoadScene (lastFinished + 1);
	}
	*/
	public void NextLevel(){
		SceneManager.LoadScene (SceneManager.GetActiveScene().buildIndex + 1);
		CanShowAds ();
	}
	/*
	public void Left(){
		print ("left");
		snake.MoveRight ();
	}
	public void Right(){
		print ("right");
		snake.MoveLeft ();
	}
*/
	public bool rotationControl = true;
	public void LeftTop(){
		//print ("leftTop");
		if (!paused) {
			if (rotationControl) {
				snake.RotateAntiClockwise ();
			}else
				snake.MoveLeftTop ();
		}
	}
	public void RightTop(){
		//print ("rightTop");
		if (!paused)
			if (rotationControl) {
				snake.RotateClockwise ();
			} else
				snake.MoveRightTop ();

	}

	public void LeftBot(){
		//print ("leftBot");
		if(!paused)
			if (rotationControl) {
				snake.RotateAntiClockwise ();
			}else
				snake.MoveLeftBot ();
	}
	public void RightBot(){
		//print ("rightBot");
		if(!paused)
			if (rotationControl) {
				snake.RotateClockwise ();
			}else
				snake.MoveRightBot ();
	}
	
	public void PlayWin(){
		audio.clip = win;
		audio.Play ();
	}
	
	public void PlayMove(){
		audio.clip = move;
		audio.Play ();
	}

	public void PlayDzin(){
		audio.clip = dzin;
		audio.Play ();
	}
	
	public void PlayPick(){
		audio.clip = pick;
		audio.Play ();
	}
	
	public void PlayLose(){
		audio.clip = lose;
		audio.Play ();
	}

	public void Unmute(bool unmute){
		this.unmute = unmute;
		//audio.mute = !unmute;
		AudioListener.pause = !unmute;
		AudioListener.volume = unmute ? 1f : 0f;
		PlayerPrefs.SetInt ("Unmute", unmute ? 1 : 0);
		UpdateAudioButton ();
		//print (PlayerPrefs.GetInt ("Unmute"));
	}



	public void ResetProgress(){
		PlayerPrefs.DeleteKey ("LastFinished");
		PlayerPrefs.DeleteAll ();
	}

	public void UpdateApplesTotal(){
		bool random = SceneManager.GetActiveScene ().name.Contains ("snakeRand");

		//var sceneName = SceneManager.GetActiveScene ().name;
		var sceneName = SceneManager.GetActiveScene().buildIndex;
		int lastLevelScore = PlayerPrefs.GetInt ("Score_" + sceneName);
		int lastLevelStars = PlayerPrefs.GetInt ("Stars_" + sceneName, 0);
		int currentStars = random ? 0 : FindObjectOfType<LevelInfo>().GetStars();
		int currentScore = FindObjectOfType<SnakeControl> ().applesEaten;
		int delta = currentScore - lastLevelScore;
		int deltaStars = currentStars - lastLevelStars;




		if (delta > 0) {
			PlayerPrefs.SetInt ("Score_" + sceneName, currentScore);
			int total = PlayerPrefs.GetInt ("Total apples");
			if(!random)
				PlayerPrefs.SetInt ("Total apples", total + delta);
		}
		if (deltaStars > 0) {
			PlayerPrefs.SetInt ("Stars_" + sceneName, currentStars);
			int total = PlayerPrefs.GetInt ("Total stars");
			if(!random)
				PlayerPrefs.SetInt ("Total stars", total + deltaStars);
		}

		//print ("Total : " + PlayerPrefs.GetInt ("Total stars", 0));
		//print ("Current : " + currentStars);
	}


	public void LevelSelectionMenu(){
		SceneManager.LoadScene ("NeuLevelSelection");
	}

	public static int GetStartsTotal(){
		return PlayerPrefs.GetInt ("Total stars");
	}

	public void ShowAd()
	{
		#if !UNITY_WEBGL
		if (Advertisement.IsReady())
		{
			Advertisement.Show();
		}
		#endif
	}

	public void CanShowAds(){
		int noAdsCount = PlayerPrefs.GetInt ("noAdsCount");
		noAdsCount++;
		if (noAdsCount >= 10) {
			noAdsCount -= 10;
			ShowAd ();
		}
		PlayerPrefs.SetInt ("noAdsCount", noAdsCount);
		print ("NO ADS COUNT : " + noAdsCount);
	}


}
