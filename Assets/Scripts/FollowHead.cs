﻿using UnityEngine;
using System.Collections;

public class FollowHead : MonoBehaviour {
	Transform snake;
	public Vector3 delta = Vector3.zero;
	// Use this for initialization
	void Start () {
		snake = FindObjectOfType<SnakeControl> ().transform;
		transform.position = getTo ();
	}
	
	// Update is called once per frame
	void Update () {
		if (snake != null) {
			Vector3 to = getTo ();
			var dir = -transform.position + to;
			var dist = Time.deltaTime / 1f;
			dist = Mathf.Min (dist, dir.magnitude);
			transform.position += dir.normalized * dist;
		}
	}

	Vector3 getTo(){
		
		var head = snake.position;
		Vector3 to = head + delta;
		return to;
	}
}
