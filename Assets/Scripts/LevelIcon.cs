﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
//[ExecuteInEditMode]
public class LevelIcon : MonoBehaviour {
	public int showNumber;
	public int levelApples;
	public string sceneName;
	public int sceneBuildId;

	public UnityEngine.UI.Text numberText;
	public UnityEngine.UI.Text applesText;

	public GameObject apple3DRoot1;
	public GameObject apple3D1;

	public GameObject apple3DRoot2;
	public GameObject apple3D2;

	public GameObject apple3DRoot3;
	public GameObject apple3D3;

	public Material unavailableApple;

	public bool random;


	public string showText;

	void Start(){
		if (random) {
			showText = Available () ? showNumber + "" : (PlayerPrefs.GetInt ("Total stars") + "/" + (showNumber - 1) * 20);
		} else {
			showText = showNumber + "";
		}
		numberText.text = showText;
		numberText.fontSize = Available () ? 50 : 30;
		int applesGotten = PlayerPrefs.GetInt ("Stars_" + sceneBuildId);
		applesText.text = "" + applesGotten + "/" + levelApples;
		GetComponent<CanvasGroup> ().alpha = (Available ()) ? 1f : 0.5f;
		GetComponent<UnityEngine.UI.Button> ().interactable = (Available ());



		if (!random) {
			if (applesGotten < 1) {
				foreach (var ren in apple3D1.GetComponentsInChildren<Renderer> ()) {
					ren.sharedMaterial = unavailableApple;
				}
			}

			if (applesGotten < 2) {
				foreach (var ren in apple3D2.GetComponentsInChildren<Renderer> ()) {
					ren.sharedMaterial = unavailableApple;
				}
			}

			if (applesGotten < 3) {
				foreach (var ren in apple3D3.GetComponentsInChildren<Renderer> ()) {
					ren.sharedMaterial = unavailableApple;
				}
			}

			apple3D1.transform.position = apple3DRoot1.transform.position;
			apple3D2.transform.position = apple3DRoot2.transform.position;
			apple3D3.transform.position = apple3DRoot3.transform.position;
		} else {
			if (apple3D1 != null) {
				Destroy (apple3D1);
				Destroy (apple3D3);
			}
			apple3D2.transform.position = apple3DRoot2.transform.position;
			if (Available ()) {
				applesText.gameObject.SetActive (true);
			}

			int lastLevelScore = PlayerPrefs.GetInt ("Score_" + sceneBuildId);
			applesText.text = lastLevelScore+"";
		}

		//temp = SceneManager.GetSceneByName (sceneName).buildIndex + "";
	}

	//public string temp;

	public bool Available(){
		//return true;
		bool result;
		if (!random) {
			result = PlayerPrefs.GetInt ("Max finished level") + 1 >= sceneBuildId;
		} else {
			result = PlayerPrefs.GetInt ("Total stars") >= (showNumber - 1)*20;
		}

		return result;
	}

	public void LoadLevel(){
		//Application.LoadLevel (sceneName);
		if (sceneName != "") {
			UnityEngine.SceneManagement.SceneManager.LoadScene (sceneName);
		} else {
			UnityEngine.SceneManagement.SceneManager.LoadScene (sceneBuildId);

		}
	}

}
