﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class HalfCircleCurve : Curve {
	public bool halfUp;
	public bool halfDown;
	public Direction dir;

	public override Vector3 GetPosAt(float t){
		t = Mathf.Clamp01 (t);
		float tOrig = t;
		t = Mathf.PI * 2 * t * 0.25f;

		Vector3 result = Vector3.zero;
		switch (dir) {
		case Direction.forward:
			result = transform.TransformPoint (Vector3.forward * (tOrig - 0.5f));
			break;
		case Direction.left:
			result = transform.TransformPoint (new Vector3 (Mathf.Cos (t) * 0.5f - 0.5f, 0f, Mathf.Sin (t) * 0.5f - 0.5f));
			break;
		case Direction.right : result =  transform.TransformPoint(new Vector3 (-Mathf.Cos (t) * 0.5f + 0.5f, 0f, Mathf.Sin (t) * 0.5f - 0.5f));break;

		case Direction.bot : result =  transform.TransformPoint(new Vector3 (0f, Mathf.Cos (t) * 0.5f - 0.5f, Mathf.Sin (t) * 0.5f - 0.5f));break;
		case Direction.top : result =  transform.TransformPoint(new Vector3 (0f, -Mathf.Cos (t) * 0.5f + 0.5f, Mathf.Sin (t) * 0.5f - 0.5f));break;

			//TODO
		case Direction.halfTop : result =  transform.TransformPoint(new Vector3(0f, 0.5f, 1f)*(tOrig-0.5f)); break;
		case Direction.halfBot : result =  transform.TransformPoint(new Vector3 (Mathf.Sin (t) * 0.5f - 0.5f, 0f, Mathf.Cos (t) * 0.5f - 0.5f));break;

		}
		if (halfUp) {
			float x = (tOrig - 0.5f) * 1f + 0.5f;
			x = Mathf.Clamp01 (x);
			//result += Vector3.up * (tOrig)*0.5f;
			result += Vector3.up * (x * x - 2* x * x * (x-1))*0.5f;
			//x * x - 2* x * x * (x-1)
		}
		if (halfDown) {
			float x = (tOrig - 0.5f) * 1f + 0.5f;
			x = Mathf.Clamp01 (x);
			//result += Vector3.up * (tOrig)*0.5f;
			result -= Vector3.up * (x * x - 2* x * x * (x-1) - 1f)*0.5f;
			//x * x - 2* x * x * (x-1)
		}

		return result;
	}

	public override Quaternion GetRotAt(float t){
		t = Mathf.Clamp01 (t);
		float tOrig = t;
		t = Mathf.PI * 2 * t * 0.25f;
		Quaternion result = Quaternion.identity;
		switch (dir) {
		case Direction.forward:
			result = transform.rotation;
			break;
		case Direction.left:
			result = Quaternion.LookRotation (transform.TransformDirection (new Vector3 (-Mathf.Sin (t), 0f, Mathf.Cos (t))), Vector3.Lerp (transform.up, Vector3.up, tOrig));
			break;
		case Direction.right:
			result = Quaternion.LookRotation (transform.TransformDirection (new Vector3 (Mathf.Sin (t), 0f, Mathf.Cos (t))), Vector3.Lerp (transform.up, Vector3.up, tOrig));
			break;

		case Direction.bot:
			{
				Vector3 up = Vector3.zero;

				if (Mathf.Abs(transform.forward.y) < 0.01f)
					up = (tOrig > 0.5f ? transform.forward : transform.up);
				else {
					Vector3 temp = transform.up + Vector3.up + transform.right;

					if (tOrig < 0.5f)
						up = Vector3.Lerp (transform.up, temp, tOrig * 2f);
					if (tOrig >= 0.5f)
						up = Vector3.Lerp (temp, Vector3.up, (tOrig - 0.5f) * 2f);


				}
				result = Quaternion.LookRotation (transform.TransformDirection (new Vector3 (0f, -Mathf.Sin (t), Mathf.Cos (t))), up);

				break;
			}
		case Direction.top:
			result = Quaternion.LookRotation (transform.TransformDirection (new Vector3 (0f, Mathf.Sin (t), Mathf.Cos (t))), (tOrig > 0.5f ? -transform.forward : transform.up));
			break;
		//TODO
		case Direction.halfTop:
			result = transform.rotation;
			break;
		case Direction.halfBot:
			result = Quaternion.LookRotation (transform.TransformDirection (new Vector3 (Mathf.Cos (t), 0f, -Mathf.Sin (t) * (halfUp ? 1 : -1f))));
			break;
		}
		
		if (halfUp) {
			float x = (tOrig - 0.5f) * 1f + 0.5f;
			x = Mathf.Clamp01 (x);
			//result += Vector3.up * (x * x - 2* x * x * (x-1))*0.5f;
			//(x * x - 2* x * x * (x-1))*0.5f
			
			if(t != 0 && t != 1)
				result = result * Quaternion.LookRotation(Vector3.up * (-x*x + x)*3f + Vector3.forward);
		}
		if (halfDown) {
			float x = (tOrig - 0.5f) * 1f + 0.5f;
			x = Mathf.Clamp01 (x);
			//result += Vector3.up * (tOrig)*0.5f;
			//result -= Vector3.up * (x * x - 2* x * x * (x-1))*0.5f;
			//x * x - 2* x * x * (x-1)
			if(t != 0 && t != 1)
				result = result * Quaternion.LookRotation(-Vector3.up * (-x*x + x)*3f + Vector3.forward);
		}
			
		return result;
	}


	public enum Direction{
		forward, left, right, bot, top, halfTop, halfBot
	}


}

